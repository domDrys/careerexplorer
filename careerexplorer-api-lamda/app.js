var express = require('express');

var indexRouter = require('./routes/index');
var searchRouter = require('./routes/search');
var submitRouter = require('./routes/submit');

var app = express();

app.use(express.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });  

app.use('/', indexRouter);
app.use('/search', searchRouter);
app.use('/submit', submitRouter);

module.exports = app;
