var bulkAddToIndex = require('./es/bulkAddToIndex');

function validateJob(job) {
    let validationErrors = [];

    if (!job.url || job.url.length < 5) {
        validationErrors.push('URL is invalid');
    }

    if (!job.hiringOrganisation || job.hiringOrganisation.length < 3) {
        validationErrors.push('Hiring Organisation is invalid');
    }

    if (!job.jobTitle || job.jobTitle.length < 3) {
        validationErrors.push('Job Title is invalid');
    }

    if (!job.location || job.location.length < 3) {
        validationErrors.push('Location is invalid');
    }

    if (!job.captchaValue) {
        validationErrors.push('captchaValue missing');
    }

    return validationErrors;
}

function submitJobAsync(job) {
    return new Promise(function (resolve, reject) {
        let indexItem = {
            id: getIdFromUrl(job.url),
            hiringOrganisation: job.hiringOrganisation,
            title: job.jobTitle,
            location: job.location,
            imageUrl: job.imageUrl,
            postedDate: new Date().toISOString(),
            url: job.url
        };

        indexItem.experiance = job.experiance.split(',').map(function (keyword) {
            return {
                subject: keyword.trim().toLowerCase(),
                amount: 0
            }
        });

        bulkAddToIndex([indexItem], function (result) {
            resolve({
                success: true,
                response: result
            });
        })
    });
}

function getIdFromUrl(url) {
    return url.replace("https://", "").replace("www.", '').replace('___', '').replace(/[^a-z0-9]/gi, '_').toLowerCase();
}

module.exports = { validateJob, submitJobAsync };