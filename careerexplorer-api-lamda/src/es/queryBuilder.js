const nonTokenisedKeywords = require('../nonTokenisedKeywords');

function buildBody(req) {
    var hiringOrganisation = req.hiringOrganisation;
    var experiance = req.experiance;
    var education = req.education;
    var facets = req.facets;

    var query = { bool: {} };

    if (hiringOrganisation || facets.hiringOrganisation) {
        applyHiringOrganisationToQuery(hiringOrganisation, facets, query);
    }

    if ((facets && facets.experiance && facets.experiance.length > 0) || (experiance && experiance.length > 0)) {
        applyExperianceToQuery(experiance, facets, query);
    }

    if (education) {
        applyEducationToQuery(education, query);
    }

    applyDateBoostToQuery(query);

    var sort = {};
    if (!query.bool.must || query.bool.must.length === 0) {
        sort = {
            postedDate: { order: "desc" },
        };
    }

    return {
        query,
        aggs: {
            experianceSubject: {
                terms: {
                    field: 'experiance.keyword',
                    size: 50
                }
            },
        },
        highlight: {
            fields: {
                'experiance.subject': {},
                'experiance.keyword': {}
            }
        },
        size: 20,
        from: req.startIndex || 0,
        sort
    };
}

function applyEducationToQuery(education, query) {
    query.bool.should = query.bool.should || [];

    if (education.level && education.level.length > 0 && education.subject && education.subject.length > 0) {
        query.bool.should.push({
            bool: {
                must: [{
                    match: {
                        "education.subject": education.subject,
                    }
                }],
                should: [{
                    match: {
                        "education.level": education.level,
                    }
                }],
            }
        });
    }
}

function applyExperianceToQuery(experiances, facets, query) {
    var experianceQuery = {};

    experianceQuery.bool = {};
    experianceQuery.bool.should = [];

    var experianceQueries = [];
    if (experiances && experiances.length > 0) {
        experianceQueries = experiances.map(function (experiance) {
            experiance.subject = experiance.subject.toLowerCase();
            var key = nonTokenisedKeywords.includes(experiance.subject) ? "experiance.keyword" : "experiance.subject";

            var match = {};
            match[key] = experiance.subject;

            return { match };
        });

        experianceQuery.bool.should = experianceQuery.bool.should.concat(experianceQueries);
    }

    var experianceFacetQueries = [];
    if (facets.experiance && facets.experiance.length > 0) {
        experianceFacetQueries = facets.experiance.map(function (expSub) {
            expSub = expSub.toLowerCase();
            var key = nonTokenisedKeywords.includes(expSub) ? "experiance.keyword" : "experiance.subject";

            var match = {};
            match[key] = expSub;

            return { match };
        });

        experianceQuery.bool.should = experianceQuery.bool.should.concat(experianceFacetQueries);
    }

    query.bool.must = query.bool.must || [];
    query.bool.must.push(experianceQuery);
}

function applyDateBoostToQuery(query) {
    query.bool.should = query.bool.should || [];

    var dateBoosts = [
        //less than a week
        {
            range: {
                postedDate: {
                    gte: "now-7d/d",
                    boost: 3
                }
            }
        },
        //less than 2 weeks
        {
            range: {
                postedDate: {
                    gte: "now-14d/d",
                    boost: 1
                }
            }
        },
        //less than 21 days
        {
            range: {
                postedDate: {
                    gte: "now-21d/d",
                    boost: 1
                }
            }
        },
        //less than 60 days
        {
            range: {
                postedDate: {
                    gte: "now-60d/d",
                    boost: 1
                }
            }
        },
        //all days
        {
            range: {
                postedDate: {
                    lte: "now-60d/d",
                    boost: 1
                }
            }
        }
    ]

    query.bool.should = query.bool.should.concat(dateBoosts);
}

function applyHiringOrganisationToQuery(hiringOrganisation, facet, query) {
    var hiringOrganisation = hiringOrganisation || facet.hiringOrganisation;

    query.bool.must = query.bool.must || [];

    query.bool.must.push({
        match: {
            "hiringOrganisation": hiringOrganisation
        }
    });
}

module.exports = { buildBody };
