var client = require('./client.js');

function getHealth() {
  return new Promise(function (resolve, reject) {
    client.cluster.health({}, function (err, resp, status) {
      if (err) {
        reject(err);
      } else {
        resolve(resp);
      }
    });
  });
}

function getCount() {
  return new Promise(function (resolve, reject) {
    client.count({ index: 'jobs', type: 'advertisement' }, function (err, resp, status) {
      if (err) {
        reject(err);
      } else {
        resolve(resp);
      }
    });
  });
}

module.exports = { getHealth, getCount};
