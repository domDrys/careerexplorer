var client = require('./client.js');
var queryBuilder = require('./queryBuilder.js');

function forKeyword(query) {
    return new Promise(function (resolve, reject) {
        client.search({
            index: 'jobs',
            type: 'advertisement',
            body: {
                query: {
                    match: { "description": query }
                }
            }
        }, function (err, response, status) {
            if (err) {
                reject(err);
            } else {
                resolve(response.hits);
            }
        });
    });
};

function forFull(bodyQuery) {
    
    console.log('req', bodyQuery);

    var body = queryBuilder.buildBody(bodyQuery);

    console.log('full', JSON.stringify(body));

    return new Promise(function (resolve, reject) {
        client.search({
            index: 'jobs',
            type: 'advertisement',
            body
        }, function (err, response, status) {
            if (err) {
                reject(err);
            } else {
                resolve(response);
            }
        });
    });
};

module.exports = { forKeyword, forFull };