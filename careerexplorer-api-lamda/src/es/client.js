require('dotenv').config();

let client = require('elasticsearch').Client({
  hosts: [ process.env.ELASTICSEARCH_URI ],
 // connectionClass: require('http-aws-es')
});

module.exports = client;  
