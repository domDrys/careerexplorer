var client = require('./client');

function bulkIndex(indexItems, onComplete) {
    var body = [];
    indexItems.forEach(function (indexItem) {
        if (indexItem) {
            var id = indexItem.id;
            delete indexItem["id"];
            body.push({ index: { _index: 'jobs', _type: 'advertisement', _id: id } });
            body.push(indexItem);
        }
    });

    client.bulk({
        index: 'jobs',
        type: 'advertisement',
        body: body
    }, function (err, resp) {
        onComplete({ err, resp });
    });
}

module.exports = bulkIndex;
