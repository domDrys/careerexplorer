

const request = require('request');


function getForUrl(url) {
    return new Promise(function (resolve, reject) {
        request(url, function (error, response, body) {
            console.log('statuscode', response && response.statusCode);

            if (response && response.statusCode && response.statusCode === 200) {
                let title, ogTitle, ogImage;

                //title
                var titleStartIndex = body.indexOf("<title>");
                if (titleStartIndex > -1) {
                    var titleEndIndex = body.indexOf("</title>", titleStartIndex);
                    title = body.substring(titleStartIndex + "<title>".length, titleEndIndex);
                    title = title.replace(/\n/g, '').replace(/\t/g, '');
                }

                //og title
                var ogtitleStart = body.indexOf("og:title")
                if (ogtitleStart > -1) {
                    var ogtitleActualStart = body.indexOf("\"", ogtitleStart + "og:title\"".length + 1) + 1;
                    var ogtitleEnd = body.indexOf("\"", ogtitleActualStart);
                    ogTitle = body.substring(ogtitleActualStart, ogtitleEnd);
                }

                //og image
                var ogImageStartIndex = body.indexOf("og:image")
                if(ogImageStartIndex){
                    var ogImageActualStart = body.indexOf("\"", ogImageStartIndex + "og:image\"".length + 1) + 1;
                    var ogImageEndIndex = body.indexOf("\"", ogImageActualStart);
                    ogImage = body.substring(ogImageActualStart, ogImageEndIndex);
                }

                resolve({
                    title,
                    ogImage,
                    ogTitle
                });
            } else {
                reject(error);
            }
        });

    })
}
module.exports = { getForUrl };