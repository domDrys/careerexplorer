var express = require('express');
var router = express.Router();
var info = require('../src/es/info');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.json({ 'status': 'ok' });
});

router.get('/health', function (req, res, next) {
  info.getHealth()
    .then(function (result) {
      res.json(result);
    }, function (err) {
      next(err);
    });
});

router.get('/count', function (req, res, next) {
  info.getCount()
    .then(function (result) {
      res.json(result);
    }, function (err) {
      next(err);
    });
});


module.exports = router;
