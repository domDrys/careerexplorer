var express = require('express');
var router = express.Router();
var search = require('../src/es/search');

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

router.post('/', function (req, res, next) {
  search.forFull(req.body)
    .then(function (result) {
      res.json(result);
    }, function (err) {
      next(err);
    });
});

module.exports = router;
