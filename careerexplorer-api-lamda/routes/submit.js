const express = require('express');
const router = express.Router();
const submitFacade = require('../src/submitFacade');
const metadataFacade = require('../src/metadataFacade');
const GoogleRecaptcha = require('google-recaptcha');
const googleRecaptcha = new GoogleRecaptcha({ secret: '6LfD73kUAAAAAE2qFhuu2mLrL72eVyK3tPDBc8dh' });

router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

router.post('/', function (req, res, next) {
    var job = req.body;

    console.log(job);

    var validationErrors = submitFacade.validateJob(job);

    if (validationErrors.length > 0) {
        res.status(400);
        return res.json({
            success: false,
            validationErrors
        });
    }


    googleRecaptcha.verify({ response: job.captchaValue }, (error) => {
        if (error) {
            res.status(400);
            return res.json({
                success: false,
                validationErrors: 'Captcha failed'
            });
        }

        submitFacade.submitJobAsync(job)
            .then((result) => res.json(result));
    });
});

router.post('/metadata', function (req, res, next) {
    var url = req.body.url;
    console.log('metadata for url:', url);

    metadataFacade.getForUrl(req.body.url)
        .then((result) => res.json(result),
            (error) => next(error))
});


module.exports = router;
