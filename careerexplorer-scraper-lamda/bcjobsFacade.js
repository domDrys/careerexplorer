require('dotenv').config();

var scrapeFacade = require('./scrapeFacade');
var indexerFacade = require('./indexer/indexerFacade');
var pushToIndex = require('./es/pushToIndex');


async function scrape(callback, event) {

    var page = event.pageToRequest || '1';
    var url = `https://www.bcjobs.ca/information-technology-jobs#page=${page}&q=&location=Vancouver%2C+BC&range=10&freshness=0&categoryIds=25&employerTypeIds=&positionTypeIds=&memberStatusIds=&careerLevelIds=&featuredEmployersOnly=false&trainingPositionsOnly=false`;

    var initalWait = process.env.INITIALWAIT;
    console.log(`Loading page ${page}...`);

    var htmlResult = null;
    try {
        htmlResult = await scrapeFacade.getHtml(url, initalWait);
    } catch (e) {
        callback(new Error('getHtml error: ' + e));
        console.error('getHtml error: ' + e);
        return;
    }

    var urlResults = [];
    try {
        urlResults = await scrapeFacade.findLinks(htmlResult.html, '.list-item-wrapper', '.list-item-wrapper .xs-text-left strong', 'https://www.bcjobs.ca');
    } catch (e) {
        callback(new Error('findLinks error: ' + e));
        console.error('findLinks error: ' + e);
        return;
    }
    
    var indexResults = [];
    urlResults.forEach(async function (urlResult) {
        let result = null;
        try {
            result = await scrapeFacade.scrapeUrl(urlResult.url, ".callout-title.u_mb-xxs", '.job-wrapper.u_p-base', '.job-wrapper.u_mb-xxs .text-limit a', null, urlResult.postedDate, null, null, '.logo-place img', 'https://www.bcjobs.ca');
        }
        catch (e) {
            console.error('scrapeUrl  error: ' + e);
            indexResults.push({urlResult, e});
            return;
        }
  
        var indexItem = indexerFacade.buildIndexItem(result);
        pushToIndex(indexItem, function (result) {
            indexResults.push({
                result,
                indexItem
            });

            if (indexResults.length === urlResults.length) {
                callback(null, { indexResults });
                return;
            }
        });
    });
};

module.exports = { scrape };
