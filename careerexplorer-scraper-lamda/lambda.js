var amazonFacade = require('./amazonFacade');
var microsoftFacade = require('./microsoftFacade');
var hootsuiteFacade = require('./hootsuiteFacade');
var unbounceFacade = require('./unbounceFacade');
var bcjobsFacade = require('./bcjobsFacade');
var indeedFacade = require('./indeedFacade');

exports.handler = function (event, context, callback) {
  switch (event.action) {
    case "AMAZON":
      amazonFacade.scrape(callback);
      break;
    case "MICROSOFT":
      microsoftFacade.scrape(callback);
      break;
    case "HOOTSUITE":
      hootsuiteFacade.scrape(callback);
      break;
    case "UNBOUNCE":
      unbounceFacade.scrape(callback);
      break;
    case "BCJOBS":
      bcjobsFacade.scrape(callback, event);
      break;
    case "INDEED":
      indeedFacade.scrape(callback, event);
      break;
    default:
      console.error('unrecognised action: ' + event.action);
      callback(new Error('unrecognised action: ' + event.action));
      break;
  }
};