const fs = require('fs');

function saveToFile(file, filename) {
    return new Promise(function (resolve, reject) {
        let pathToFile = `C:/dump/${filename}.json`;
        fs.writeFile(pathToFile, JSON.stringify(file), function (err) {
            if (err) {
                reject(err);
                return;
            }

            resolve(pathToFile);
        });
    })
}

module.exports = saveToFile;