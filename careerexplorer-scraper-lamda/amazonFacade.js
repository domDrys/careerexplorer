require('dotenv').config();

var scrapeFacade = require('./scrapeFacade');
var indexerFacade = require('./indexer/indexerFacade');
var pushToIndex = require('./es/pushToIndex');
const imageUrl = 'https://static.amazon.jobs/global_images/2/images/Jobs_thumbnailV2.jpg?1554491962';
const url = 'https://www.amazon.jobs/en/search?offset=0&result_limit=10&sort=recent&category=software-development&distanceType=Mi&radius=24km&latitude=49.26039&longitude=-123.11336&loc_group_id=&loc_query=Vancouver%2C%20BC%2C%20Canada&base_query=&city=Vancouver&country=CAN&region=British%20Columbia&county=Greater%20Vancouver&query_options=&';

async function scrape(callback) {
    var initalWait = process.env.INITIALWAIT;
    console.log('Loading initial page...');

    var htmlResult = null;
    try {
        htmlResult = await scrapeFacade.getHtml(url, initalWait);
    } catch (e) {
        callback(new Error('getHtml error: ' + e));
        console.error('getHtml error: ' + e);
        return;
    }

    var urlResults = [];
    try {
        urlResults = await scrapeFacade.findLinks(htmlResult.html, '.job-link', '.posting-date', 'https://www.amazon.jobs');
    } catch (e) {
        callback(new Error('findLinks error: ' + e));
        console.error('findLinks error: ' + e);
        return;
    }

    var indexResults = [];
    urlResults.forEach(async function (urlResult) {
        let result = null;
        try {
            result = await scrapeFacade.scrapeUrl(urlResult.url, ".title", '#main-content .content', null, null, urlResult.postedDate, 'Amazon', imageUrl);
        }
        catch (e) {
            indexResults.push({
                result,
                urlResults
            });
            console.error('scrapeUrl  error: ' + e);
            return;
        }

        var indexItem = indexerFacade.buildIndexItem(result);
        pushToIndex(indexItem, function (result) {
            indexResults.push({
                result,
                indexItem
            });

            if (indexResults.length === urlResults.length) {
                callback(null, { indexResults });
                return;
            }

            console.log('SUCCESS', JSON.stringify({
                result,
                indexItem
            }));
        });
    });
};

module.exports = { scrape };
