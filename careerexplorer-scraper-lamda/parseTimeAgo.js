function parseTimeAgo(timeString) {

    if(!timeString){
        return null;
    }

    var number = parseInt(timeString.replace(/^[0-9]+$/g, ''));

    if(timeString.indexOf('day')){
        var d = new Date();
        
        d.setDate(d.getDate() - number);

        return d.toISOString();
    } else if(timeString.indexOf('day')){
        return new Date().toISOString();
    } else {
        console.log('unhandled time string: ' + timeString);
        return null;
    }
}

module.exports = parseTimeAgo;