const indexerFacade = require('../indexer/indexerFacade');
const { performance, PerformanceObserver } = require('perf_hooks');
const fs = require('fs');


function readFiles(dirname, onError) {
    return new Promise(function (resolve, reject) {
        var results = [];

        fs.readdir(dirname, function (err, filenames) {
            if (err) {
                onError(err);
                return;
            }
            filenames.forEach(function (filename) {
                fs.readFile(dirname + '/' + filename, 'utf-8', function (err, content) {
                    if (err) {
                        console.error(err);
                        onError(err);
                        return;
                    }
                    results.push(JSON.parse(content));

                    if (results.length == filenames.length) {
                        resolve(results);
                    }
                });
            });
        });
    });
}



readFiles('C:/dump', (e) => console.error(e))
    .then(function (results) {

        var t0 = performance.now();

        var indexItems = results.map(result => indexerFacade.buildIndexItem(result));

        var t1 = performance.now();

        console.log(`Built ${results.length} indexItems in ${(t1 - t0)} milliseconds.`);
    });


