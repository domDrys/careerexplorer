require('dotenv').config();

var scrapeFacade = require('./scrapeFacade');
var indexerFacade = require('./indexer/indexerFacade');
var pushToIndex = require('./es/pushToIndex');
const url = 'https://careers.microsoft.com/us/en/search-results?rk=l-vancouver&rt=professional';

function scrape(callback){
    var initalWait = process.env.INITIALWAIT;
    console.log('Loading initial page...');
    
    scrapeFacade.getHtml(url, initalWait).then(function (result) {
        var html = result.html;
        var startIndex = html.indexOf('{"widgetApiEndpoint"');
        var endIndex = html.indexOf('/*', startIndex);
        var jsonStrings = html.substring(startIndex, endIndex);

        var jobDatas = [];
        var getHtmlActions = [];
        jsonStrings.split(' = {').forEach(function (json) {
            json = "{" + json;
            json = json.substring(0, json.lastIndexOf('}') + 1);

            var jobIds = [];
            try {
                var obj = JSON.parse(json);
                if (obj.eagerLoadRefineSearch) {
                    jobDatas = jobDatas.concat(obj.eagerLoadRefineSearch.data.jobs.map(function (job) {
                        return {
                            jobID: job.jobId,
                            postedDate: job.postedDate
                        }
                    }));

                    jobIds = jobIds.concat(obj.eagerLoadRefineSearch.data.jobs.map((job) => job.jobId));
                }
            } catch (e) { }
            var pageWait = process.env.PAGEWAIT;

            let newActions = jobIds.map((url) => scrapeFacade.getHtml(`https://careers.microsoft.com/us/en/job/${url}`, pageWait));
            getHtmlActions = getHtmlActions.concat(newActions);
        });

        console.log('got urls', getHtmlActions.length + 'got jobdatas' + jobDatas.length);
        let results = [];
        getHtmlActions.forEach(function (action) {
            action.then(function (result) {
                var jobData = jobDatas.find((d) => result.url.indexOf(d.jobID) > -1);

                var jobObj = getJobObjFromHtmlMicrosoft(result.html, result.url, jobData.postedDate);

                var indexItem = indexerFacade.buildIndexItem(jobObj);
                pushToIndex(indexItem, function (result) {
                    results.push({
                        result, 
                        indexItem
                    });

                    if (results.length === getHtmlActions.length) {
                        callback(null, { results });
                    }

                    console.log('SUCCESS', JSON.stringify({
                        result,
                        indexItem
                    }));
                });   
            });
        });
    }, function (e) {
        callback(new Error('getHtml error: ' + e));
        console.error('getHtml error: ' + e);
    });
}

function getJobObjFromHtmlMicrosoft(jobHtml, jobUrl, postedDate) {
    var start = jobHtml.indexOf("{\"siteConfig");
    var end = jobHtml.indexOf("};", start);
    var jobJson = jobHtml.substring(start, end + 1);

    var jobObj = JSON.parse(jobJson).jobDetail.data.job;

    var job = {
        title: jobObj.title,
        description: jobObj.description + jobObj.jobQualifications + jobObj.jobResponsibilities,
        hiringOrganization: jobObj.companyName,
        url: jobUrl,
        id: jobUrl.replace("https://.", "").replace('www', '').replace(/[^a-z0-9]/gi, '_').toLowerCase(),
        postedDate,
        imageUrl: 'https://prodcmscdn.azureedge.net/careerconnectresources/p/MICRUS/social/Share_msft-1526996096885.jpg'
    };

    return job;
}

module.exports = { scrape };