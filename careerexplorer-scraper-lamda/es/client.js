require('dotenv').config();

console.log('ELASTICSEARCH_URI:' + process.env.ELASTICSEARCH_URI);

let client = require('elasticsearch').Client({
  hosts: [process.env.ELASTICSEARCH_URI],
  // connectionClass: require('http-aws-es')
});

module.exports = client;  
