var client = require('./client');

function pushToIndex(indexItem, onComplete) {
    var validationErrors = getValidationErrors(indexItem);

    if (validationErrors.length > 0) {
        onComplete({ err: 'validation errors', resp: validationErrors });
        return;
    }

    var body = [];
    var id = indexItem.id;
    delete indexItem["id"];
    body.push({ index: { _index: 'jobs', _type: 'advertisement', _id: id } });
    body.push(indexItem);

    client.bulk({
        index: 'jobs',
        type: 'advertisement',
        body: body
    }, function (err, resp) {
        onComplete({ err, resp });
    });
}


function getValidationErrors(indexItem) {
    var errors = [];

    if (!indexItem.title || indexItem.title.length === 0) {
        errors.push('Missing title');
    } else if ([';', '#'].some((char) => indexItem.title.indexOf(char) > -1)) {
        errors.push('contains illegal char');
    }

    if (!indexItem.experiance || indexItem.experiance.length === 0) {
        errors.push('missing experiance');
    } else if (indexItem.experiance.length < 4) {
        errors.push('not enough experiance keywords');
    }

    if (!indexItem.hiringOrganisation || indexItem.hiringOrganisation.length === 0) {
        errors.push('Missing hiringOrganisation');
    }

    return errors;
}

module.exports = pushToIndex;
