var client = require('./client');

module.exports = async function (itemIds) {
    const response = await client.mget({
        index: 'jobs',
        type: 'advertisement',
        body: {
            ids: itemIds
        }
    });

    return response;
};

