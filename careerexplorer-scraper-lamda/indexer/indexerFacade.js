var parseExperiance = require('./parseExperiance');

function buildIndexItem(content) {
    var id = getIdFromUrl(content.url);

    var title = content.title;

    var experianceParse = parseExperiance.parse(content.description, id);

    return {
        id,
        hiringOrganisation: content.hiringOrganization ? content.hiringOrganization.toLowerCase() : '',
        title,
        experiance: experianceParse.experiance,
        url: content.url,
        postedDate: content.postedDate,
        location: 'vancouver',
        imageUrl: content.imageUrl
    };
}

function getIdFromUrl(url){
    return url.replace("https://www.", "").replace(/[^a-z0-9]/gi, '_').toLowerCase();
}

module.exports = { buildIndexItem, getIdFromUrl };