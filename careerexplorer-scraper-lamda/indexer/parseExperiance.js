const rake = require('node-rake');
const parse5 = require('parse5');
const badKeyWords = require('./badKeywords');
const techKeywords = require('./techKeywords');
const striptags = require('striptags');

function parse(html, id) {
    var experiance = [];
    var experianceStrings = [];
    var contentSplit = striptags(html, [], "$_")
        .split("$_")
        .map((exp) => exp.replace(/&#x2019;/g, '\'').replace(/&#x2022;/g, '').replace(/&#xB7;/g, '').replace('- ', '').toLowerCase().trim());


    if (!contentSplit || !html || !id) {
        console.error('parseExperiance for ' + id + ' failed. missing data: contentSplit:' + contentSplit + " html:" + html);
        return {
            experiance,
            experianceStrings,
        };
    }

    const document = parse5.parse(html);

    var experianceNodes = getExperianceNodes(document);
    var topics = [].concat(techKeywords);

    experianceNodes.forEach(function (expNode) {
        let newStrings = getExperianceStringsFromNode(expNode, id);

        experianceStrings = experianceStrings.concat(newStrings);
    });

    experianceStrings = experianceStrings.filter(onlyUnique);

    experianceStrings.forEach(function (expStr) {
        let keywords = rake.generate(expStr);


        var keyWordToSplitIndex = keywords.findIndex((key) => key.indexOf(',') > -1);
        while (keyWordToSplitIndex > -1) {
            var split = keywords[keyWordToSplitIndex].split(',');
            keywords.splice(keyWordToSplitIndex, 1, ...split);

            keyWordToSplitIndex = keywords.findIndex((key) => key.indexOf(',') > -1);
        }

        keywords = keywords.map((keyword) => cleanKeyword(keyword)).filter((key) => filterKeyword(key, experiance));

        topics = topics.concat(keywords);
    });

    topics = topics.filter(onlyUnique).filter((key) => key && key.length > 0);

    topics
        .filter((topic) => !experiance.some((exp) => exp.subject === topic)) //not already in list
        .forEach(function (topic) {
            var amount = 0;
            var found = false;
            contentSplit.forEach(function (c) {
                c = c.toLowerCase().replace(/\//g, ' ').replace(/,/g, ' ');
                if (ifStringContainsKeyword(c, topic)) {
                    found = true;
                    var newAmt = getNumberFromString(c);
                    if (newAmt && amount === 0 && newAmt > 0) {
                        amount = newAmt;
                    }
                }
            });

            if (found) {
                experiance.push({
                    keyword: topic,
                    subject: topic,
                    amount
                });
            }
        });


    return {
        experiance,
        experianceStrings,
    };
}

function ifStringContainsKeyword(c, topic) {
    if (c.indexOf(` ${topic} `) > -1 || c.indexOf(` ${topic}.`) > -1) {
        return true;
    }

    if (c.endsWith(topic) || c.endsWith(`${topic}.`)) {
        return true;
    }

    if (c.startsWith(`${topic} `)) {
        return true;
    }
}

function cleanKeyword(keyword) {
    keyword = keyword.toLowerCase();

    if (keyword.endsWith('\'') || keyword.endsWith(',') || keyword.endsWith(';') || keyword.endsWith('’') || keyword.endsWith(')') || keyword.endsWith('(')) {
        keyword = keyword.substring(0, keyword.length - 1);
    }

    if (keyword.endsWith('+ years')) {
        keyword = keyword.substring(0, keyword.length - '+ years'.length);
    }

    if (keyword.endsWith(' years')) {
        keyword = keyword.substring(0, keyword.length - ' years'.length);
    }

    if (keyword.endsWith(' weeks')) {
        keyword = keyword.substring(0, keyword.length - ' weeks'.length);
    }

    if (keyword.endsWith(' year')) {
        keyword = keyword.substring(0, keyword.length - ' year'.length);
    }

    if (keyword.endsWith('experience')) {
        keyword = keyword.substring(0, keyword.length - 'experience'.length);
    }

    if (keyword.startsWith('including')) {
        keyword = keyword.substring('including'.length);
    }

    return keyword.trim();
}

const letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

function filterKeyword(keyword, experience) {
    if (!keyword) {
        return false;
    }

    if (experience && experience.some((exp) => exp.subject === keyword)) {
        return false;
    }

    if (badKeyWords.some((key) => key === keyword)) {
        return false;
    }

    var lettersInKeyword = [];
    let i = keyword.length;
    while (i--) {
        if (letters.some((l) => l === keyword.charAt(i).toLowerCase())) {
            lettersInKeyword.push(keyword.charAt(i).toLowerCase());
        }
    }

    if (lettersInKeyword.length === 0) {
        return false;
    }

    if (keyword && keyword.length < 3) {
        try {
            if (parseInt(keyword) > 0) {
                return false;
            }
        }
        catch (e) {

        }
    }

    var months = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'september', 'october', 'november', 'december'];

    if(months.some((month) => keyword.indexOf(month) > -1)){
        return false;
    }

    return true;
}

function getExperianceNodes(node) {
    var results = [];

    if (!node.childNodes) {
        return results;
    }

    node.childNodes.forEach(function (child) {
        if (isNodeValueGood(child)) {
            results.push(child);
        } else {
            results = results.concat(getExperianceNodes(child));
        }
    });

    return results;
}

function getExperianceStringsFromNode(node, id) {
    var keywords = [];

    var values = getAllValuesInTree(node.parentNode.parentNode);

    var badValues = [];

    values.forEach(function (val) {
        try {
            let newKeywords = rake.generate(val);

            if (newKeywords && newKeywords.length) {
                keywords = keywords.concat(newKeywords);
                keywords = keywords.filter(onlyUnique);
            }
        } catch (e) {
            console.log(id + ': getExperianceStringsFromNode failure: ', val);
            badValues.push(val);
        }
    });

    if (node.parentNode.parentNode.parentNode && keywords < 15) {
        return getExperianceStringsFromNode(node.parentNode.parentNode, id);
    } else {
        return values.filter((v) => !badValues.some((b) => b === v));
    }
}


function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function cleanNodeValue(value) {
    value = value
        .replace(/"/g, '')
        .replace('i.e.', '')
        .replace(/&#x2019;/g, '\'')
        .replace(/&#x2022;/g, '')
        .replace(/&#xB7;/g, '')
        .replace(/&apos;/g, '\'')
        .replace(/- /g, '')
        .replace(/•/g, '.')
        .replace(/\s\s+/g, ' ')
        .replace(/·/g, '.')
        .replace(/\(/g, '')
        .replace(/\)/g, '')
        .replace('…', '.')
        .replace(/\//, 'or')
        .replace(/  +/g, ' ')
        .trim().toLowerCase();

    if (value.startsWith('-') || value.startsWith('*') || value.startsWith('.') || value.startsWith('·')) {
        value = value.substring(1).trim();
    }

    return value;
}

function getAllValuesInTree(node) {
    var results = [];

    if (node.value) {
        node.value = cleanNodeValue(node.value);
        if (node.value && node.value.length > 1) {
            results.push(node.value);
        }
    }

    if (node.childNodes) {
        var childStringSets = node.childNodes.map((child) => getAllValuesInTree(child));
        results = [].concat.apply([], childStringSets);
    }

    return results;
}

function isNodeValueGood(node) {
    if (node.value && node.value.length > 3) {
        node.value = node.value.trim().toLowerCase();
        if (node.value.indexOf('qualifications') > -1 || node.value.indexOf('experiance') > -1 || node.value.indexOf('experience') > -1) {
            return true;
        }
    }
    return false;
}

function getNumberFromString(str) {
    var amounts = str.match(/^\d+|\d+\b|\d+(?=\w)/g);
    if (amounts && amounts.length > 0) {
        return parseInt(amounts[0]);
    } else {
        return null;
    }
}

module.exports = { parse, filterKeyword, cleanNodeValue };
