const badKeyWords = [
    'equivalent',
    'considered',
    'living',
    'relocate',
    'years',
    'bachelor',
    'degree',
    'basic qualifications',
    'knowledge',
    'experience',
    'years', 'experience',
    'work',
    'ability',
    'things',
    'preferred qualifications',
    'relevant work',
    'performance',
    'ready',
    'sort',
    'person',
    'world',
    'offer',
    'join',
    'benefits',
    'speed',
    'accuracy',
    'roll',
    'system',
    'ancestry',
    'age',
    'country',
    'employment',
    'customers',
    'drive',
    'nature',
    'equal opportunity employer',
    'build',
    'support',
    'description',
    'amplify',
    'driven',
    'based',
    'prospects',
    'meet',
    'related field',
    'rewards',
    'bachelors',
    'strong',
    'passionate',
    'computer',
    'software development',
    'computer science',
    'team',
    'practices',
    'support',
    'design',
    'business',
    'services',
    'color',
    'applicable laws',
    'characteristic protected',
    'expression',
    'family',
    'gender identity',
    'genetic information',
    'marital status',
    'medical care leave',
    'medical condition',
    'field',
    'role',
    'time',
    'leave',
    'development',
    'mental disability',
    'national origin',
    'ordinances',
    'physical',
    'political affiliation',
    'protected veteran status',
    'race',
    'qualified applicants',
    'receive consideration',
    'regard',
    'regulations',
    'religion',
    'sexual orientation',
    'opportunity',
    'vary depending',
    'disability',
    'building',
    'teams',
    'achieve',
    'impact',
    'working',
    'canada',
    'resources',
    'make',
    'career',
    'quality',
    'collaboration',
    'technology',
    'communication',
    'skills',
    'success',
    'deliver',
    'education',
    'create',
    'results',
    'belief',
    'engage',
    'including',
    'passion',
    'individual',
    'experiences',
    'level',
    'maintain',
    'data',
    'technical',
    'products',
    'delivering',
    'company',
    'solve',
    'vancouver',
    'initiatives',
    'seeking',
    'application',
    'expertise',
    'background',
    'preferred',
    'developers',
    'culture',
    'collaborative',
    'software',
    'learn',
    'realize',
    'software',
    'committed',
    'located',
    'consistent',
    'criminal histories',
    'dynamic',
    'mission',
    'required',
    'place',
    'part',
    'millions',
    'productive',
    'solid grasp',
    'modern workplace',
    'small',
    'provide',
    'understanding',
    'growth mindset',
    'ecosystem',
    'great',
    'inclusive',
    'collaborating',
    'provide accommodations requested',
    'candidates taking part',
    'inclusive employer',
    'selection process',
    'effort',
    'made',
    'aspects',
    'understand',
    'share',
    'experienced',
    'public',
    'creating',
    'ensuring',
    'candidates',
    'bonus',
    'large',
    'users',
    'highly',
    'load',
    'times',
    'succeed',
    'led',
    'ways',
    'improvements',
    'multiple',
    'block',
    'fail',
    'myriad',
    'successful',
    'leading',
    'lead',
    'product',
    'support',
    'comfortable',
    'resourceful',
    'experience working',
    'responsible',
    'focus',
    'easy',
    'simplify',
    'challenge',
    'yearly prime membership',
    'grow',
    'customer',
    'projects',
    'love',
    'turn',
    'ideas',
    'top',
    'put',
    'make',
    'member',
    'demonstrated',
    'dollars',
    'quickly',
    'sketch',
    'resume',
    'tools',
    'find',
    'helping',
    'people',
    'support',
    'gauging',
    'infuse',
    'abilities',
    'organization',
    'provided',
    'gender',
    'ensure',
    'veteran',
    'day',
    'bc',
    'full time',
    'north vancouver',
    'apply',
    'attention',
    'detail',
    'similar',
    'female',
    'proficiency',
    'weeks',
    'code',
    'demonstrated ability',
    'experience building',
    'hour',
    'features',
    'implementation',
    'excellent written',
    'including coding standards'
];

module.exports = badKeyWords;
