require('dotenv').config();

var scrapeFacade = require('./scrapeFacade');
var indexerFacade = require('./indexer/indexerFacade');
var pushToIndex = require('./es/pushToIndex');
const url = 'https://careers.hootsuite.com/global/en/vancouver';

function scrape(callback){
    var initalWait = process.env.INITIALWAIT;

    scrapeFacade.getHtml(url, initalWait)
        .then(function (result) {
            var html = result.html;
            var startIndex = html.indexOf('{"widgetApiEndpoint"');
            var endIndex = html.indexOf('/*', startIndex);
            var jsonStrings = html.substring(startIndex, endIndex);
            let jobDatas = [];
            var getHtmlActions = [];
            jsonStrings.split(' = {').forEach(function (json, index) {
                json = "{" + json;
                json = json.substring(0, json.lastIndexOf('}') + 1);

                let urls = []

                try {
                    var obj = JSON.parse(json);
                    if (obj.eagerLoadRefineSearch) {
                        jobDatas = jobDatas.concat(obj.eagerLoadRefineSearch.data.jobs.map(function (job) {
                            return {
                                jobID: job.jobId,
                                postedDate: job.postedDate
                            }
                        }));

                        urls = obj.eagerLoadRefineSearch.data.jobs.map((job) => job.applyUrl);
                    }
                } catch (e) { }
                var pageWait = process.env.PAGEWAIT;
                getHtmlActions = getHtmlActions.concat(urls.map((url) => scrapeFacade.getHtml(url, pageWait)));
            });

            let results = [];
            getHtmlActions.forEach(function (action) {
                action.then(function (result) {
                    var jobHtml = result.html;
                    var jobUrl = result.url;

                    var start = jobHtml.indexOf("{\"hiringOrganiz");
                    var end = jobHtml.indexOf("</script>", start);
                    var jobJson = jobHtml.substring(start, end);

                    var jobData = jobDatas.find((d) => result.url.indexOf(d.jobID) > -1);

                    var jobObj = getJobObjFromHtmlMicrosoft(result.html, result.url, jobData.postedDate);

                    try {
                        var jobObj = JSON.parse(jobJson);

                        var job = {
                            title: jobObj.title,
                            description: jobObj.description,
                            hiringOrganization: jobObj.hiringOrganization.name,
                            url: jobUrl,
                            postedDate: jobData.postedDate,
                            imageUrl: 'https://assets.phenompeople.com/CareerConnectResources/pp/HOOTUS/social/hootstuie_shared_logo_image-1514874608922.png'
                        };

                        var indexItem = indexerFacade.buildIndexItem(job);
                        pushToIndex(indexItem, function (result) {
                            results.push({
                                result, 
                                indexItem
                            });
        
                            if (results.length === getHtmlActions.length) {
                                callback(null, { results });
                            }
        
                            console.log('SUCCESS', JSON.stringify({
                                result,
                                indexItem
                            }));
                        });   
                    } catch (e) { }

                }, function (e) {
                    callback(new Error('getURL error: ' + e));
                    console.error('getURL error: ' + e);
                });;
            });
        }, function (e) {
            callback(new Error('getHTML error: ' + e));
            console.error('getHTML  error: ' + e);
        });
};

function getJobObjFromHtmlMicrosoft(jobHtml, jobUrl, postedDate) {
    var start = jobHtml.indexOf("{\"siteConfig");
    var end = jobHtml.indexOf("};", start);
    var jobJson = jobHtml.substring(start, end + 1);

    var jobObj = JSON.parse(jobJson).jobDetail.data.job;

    var job = {
        title: jobObj.title,
        description: jobObj.description + jobObj.jobQualifications + jobObj.jobResponsibilities,
        hiringOrganization: jobObj.companyName,
        url: jobUrl,
        id: jobUrl.replace("https://.", "").replace('www', '').replace(/[^a-z0-9]/gi, '_').toLowerCase(),
        postedDate,
        imageUrl: 'https://prodcmscdn.azureedge.net/careerconnectresources/p/MICRUS/social/Share_msft-1526996096885.jpg'
    };

    return job;
}

module.exports = { scrape };
