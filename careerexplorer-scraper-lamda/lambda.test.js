var lambda = require('./lambda');

lambda.handler({ action: "AMAZON", pageToRequest: 1, pageSize : 20 }, {}, function (err, result) {
    if (err) {
        return console.error(JSON.stringify(err));
    }
    console.log('ok');
    console.log(JSON.stringify(result));
    return;
});