require('dotenv').config();

var scrapper = require('./scraper');
const cheerio = require('cheerio');
const striptags = require('striptags');

function getHtml(url, wait) {
    return new Promise(function (resolve, reject) {
        console.log('getHtml: ' + url + ' with wait:' + wait);
        scrapper.scrape(url, wait, function (err, html) {
            console.log('getHtml finished. ' + url + ' error: ' + err + ' result length: ' + (html ? html.length : 0));
            if (err) {
                reject(err);
            } else {
                resolve({ html, url });
            }
        });
    });
}

function findLinksWithParams(html, baseUrl, selectors) {
    return new Promise(function (resolve, reject) {
        if (!html || !html.length) {
            reject('html was null or empty');
        }

        if (!selectors.object || !selectors.link) {
            reject('missing required selector');
        }

        var $ = cheerio.load(html);

        var urls = [];
        $(selectors.object).filter(function () {

            var url = $(this).find(selectors.link).attr("href");

            var hiringOrg = null;
            if (selectors.hiringOrg) {
                hiringOrg = $(this).find(selectors.hiringOrg).text().replace('\\n', '').trim();
            }

            var postedDate = null;
            if (selectors.postedDate) {
                postedDate = $(this).find(selectors.postedDate).text();
            }

            if (url) {
                urls.push({
                    postedDate,
                    url : (baseUrl || '') + url,
                    hiringOrg,
                });
            }
        });

        if (urls.length === 0) {
            console.error(`no links found using selector: ${selectors.object}. html length: ${(html ? html.length : 0)}`);
            reject(`no links found using linkSelector: ${selectors.object}`);
        } else {
            console.log(`found ${urls.length} links`);
            resolve(urls);
        }
    });
}

function findLinks(html, linkSelector, postedDateSelector, baseUrl) {
    return new Promise(function (resolve, reject) {
        if (!html || !html.length) {
            reject('html was null or empty');
        }

        var $ = cheerio.load(html);

        var urls = [];
        $(linkSelector).filter(function () {
            var url = `${baseUrl || ''}${$(this).attr("href")}`;

            urls.push({
                postedDate: $(this).find(postedDateSelector).text().replace('Posted ', ''),
                url,
            });
        });

        if (urls.length === 0) {
            console.error(`no links found using linkSelector: ${linkSelector}. html length: ${(html ? html.length : 0)}`);
            reject(`no links found using linkSelector: ${linkSelector}`);
        } else {
            console.log(`found ${urls.length} links`);
            resolve(urls);
        }
    });
}

function scrapeUrlWithParms(url, params){
    return scrapeUrl(url, params.titleSelector, params.descriptionSelector, params.hiringOrganizationSelector, params.postedDateSelector, params.postedDate, params.hiringOrganization, params.imageUrl, params.imageUrlSelector, params.imageUrlPrefix);
}

function scrapeUrl(url, titleSelector, descriptionSelector, hiringOrganizationSelector, postedDateSelector, postedDate, hiringOrganization, imageUrl, imageUrlSelector, imageUrlPrefix) {
    var pageWait = process.env.PAGEWAIT;

    return new Promise(function (resolve, reject) {
        getHtml(url, pageWait).then(function (result) {
            var html = result.html;

            if (!html || !html.length) {
                reject('scrape url failed to load html data for: ' + url);
            }

            var $ = cheerio.load(html);

            var title = null;
            $(titleSelector).filter(function () {
                title = striptags($(this).html());
                title = title
                    .replace(/&amp;/g, '&')
                    .replace(/&#xA0;/g, ' ')
                    .replace(/\n/g, '')
                    .replace(/&apos;/g, '\'');
            });

            var description = null;
            $(descriptionSelector).filter(function () {
                description = $(this).html();
            });

            if (!hiringOrganization) {
                $(hiringOrganizationSelector).filter(function () {
                    hiringOrganization = striptags($(this).html()).replace(/\t/g, '').replace(/\n/g, '').replace('chevron-thick', '');
                });
            }

            if (!postedDate) {
                $(postedDateSelector).filter(function () {
                    postedDate = striptags($(this).html());
                });
            }

            if (postedDate) {
                try {
                    var dateObj = new Date(postedDate);
                    postedDate = dateObj.toISOString();
                } catch (e) {
                    console.error('Bad posted date:' + postedDate + ' for url: ' + url);
                }
            }

            if (imageUrlSelector) {
                $(imageUrlSelector).filter(function () {
                    imageUrl = (imageUrlPrefix || '') + $(this).attr("src");
                });
            }

            resolve({ title, description, url, hiringOrganization, postedDate, imageUrl });
        }, (e) => reject(e));
    });
};

module.exports = { getHtml, findLinks, findLinksWithParams, scrapeUrl, scrapeUrlWithParms };
