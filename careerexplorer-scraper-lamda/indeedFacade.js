require('dotenv').config();

var scrapeFacade = require('./scrapeFacade');
var indexerFacade = require('./indexer/indexerFacade');
var pushToIndex = require('./es/pushToIndex');
var parseTimeAgo = require('./parseTimeAgo');

async function scrape(callback, event) {

    var pageToRequest = event.pageToRequest || '1';
    var pageSize = event.pageSize || '20';
    var start = (parseInt(pageToRequest) - 1) * pageSize;

    let url = `https://www.indeed.ca/jobs?as_and=&as_phr=&as_any=software+programmer+technology+java+c%23+react+javascript+c%2B%2B+development&as_not=&as_ttl=&as_cmp=&jt=all&st=&as_src=&salary=&radius=10&l=Vancouver%2C+BC&fromage=7&limit=${pageSize}&start=${start}&sort=&psf=advsrch`;

    var initalWait = process.env.INITIALWAIT;
    console.log('Loading initial page...');

    var htmlResult = null;
    try {
        htmlResult = await scrapeFacade.getHtml(url, initalWait);
    } catch (e) {
        callback(new Error('getHtml error: ' + e));
        console.error('getHtml error: ' + e);
        return;
    }

    var urlResults = [];
    try {
        urlResults = await scrapeFacade.findLinksWithParams(htmlResult.html, 'https://www.indeed.ca', {
            object: '.jobsearch-SerpJobCard',
            link: 'h2 a',
            hiringOrg: '.company a',
            postedDate: '.date'
        });
    } catch (e) {
        callback(new Error('findLinks error: ' + e));
        console.error('findLinks error: ' + e);
        return;
    }

    var indexResults = [];
    urlResults.forEach(async function (urlResult) {
        let result = null;
        try {
            result = await scrapeFacade.scrapeUrlWithParms(urlResult.url, {
                titleSelector: ".jobsearch-JobInfoHeader-title",
                descriptionSelector: '.jobsearch-JobComponent-description',
                hiringOrganization: urlResult.hiringOrg,
                hiringOrganizationSelector: '.jobsearch-InlineCompanyRating',
                postedDate: parseTimeAgo(urlResult.postedDate),
                imageUrlSelector: ".jobsearch-CompanyAvatar-image"
            });
        }
        catch (e) {
            indexResults.push({ result: e });
            return;
        }

        if (result.hiringOrganization.indexOf('-') > -1) {
            result.hiringOrganization = result.hiringOrganization.split('-')[0];
        }

        var indexItem = indexerFacade.buildIndexItem(result);
        pushToIndex(indexItem, function (result) {
            indexResults.push({
                result,
                indexItem
            });

            if (indexResults.length === urlResults.length) {
                callback(null, { indexResults });
                return;
            } else {
                console.log(`finished ${indexResults.length} of ${urlResults.length}`)
            }
        });
    });
};

module.exports = { scrape };
