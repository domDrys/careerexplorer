require('dotenv').config();

var scrapeFacade = require('./scrapeFacade');
var indexerFacade = require('./indexer/indexerFacade');
var pushToIndex = require('./es/pushToIndex');
const imageUrl = 'https://unbounce-wpengine.netdna-ssl.com/photos/Unbounce-primary-logo-light-background.png';
const url = 'https://careers.unbounce.com';
var getExistingItems = require('./es/getExistingItems');

function scrape(callback) {
    var initalWait = process.env.INITIALWAIT;
    console.log('Loading initial page...');

    scrapeFacade.getHtml(url, initalWait).then(function (result) {
        scrapeFacade.findLinks(result.html, 'a', null, '')
            .then(function (urlResults) {
                urlResults = urlResults.filter(a => a.url.indexOf('careers.unbounce') > -1);

                urlResults.forEach(function(urlResult){
                    urlResult.url = urlResult.url.replace('clkn/https/', 'https://');
                });

                var ids = urlResults.map((r) => indexerFacade.getIdFromUrl(r.url));
                console.log(ids);
                getExistingItems(ids).then(result => {
                    var existingItems = [];
                    result.docs.forEach(function (doc) {
                        if (doc && doc.found) {
                            existingItems.push({
                                id: doc._id,
                                postedDate: doc._source.postedDate
                            });
                        }
                    });
                    console.log('got existingItems:' + existingItems.length);
                    var indexResults = [];
                    urlResults.forEach(function (urlResult) {
                        scrapeFacade.scrapeUrl(urlResult.url, "#lp-pom-text-26", '.lp-pom-root', null, null, new Date().toISOString(), 'Unbounce', imageUrl)
                            .then(function (result) {
                                var indexItem = indexerFacade.buildIndexItem(result);

                                var existingItem = existingItems.find((i) => i.id === indexItem.id);

                                if (existingItem) {
                                    console.log('found existing item for: ' + indexItem.id);
                                    indexItem.postedDate = existingItem.postedDate;
                                } else {
                                    console.log('no existing item found for ' + indexItem.id);
                                }

                                if (indexItem.experiance.length > 0 && indexItem.title) {
                                    pushToIndex(indexItem, function (result) {
                                        indexResults.push({
                                            result,
                                            indexItem
                                        });

                                        if (indexResults.length === urlResults.length - 1) {
                                            callback(null, { indexResults });
                                        } else {
                                            console.log(`loaded ${indexResults.length} of ${urlResults.length}`)
                                        }

                                        console.log('SUCCESS', JSON.stringify({
                                            result,
                                            indexItem
                                        }));
                                    });
                                } else {
                                    indexResults.push({
                                        result: 'skipped: bad data',
                                        indexItem: {}
                                    });
                                }
                            }, function (e) {
                                indexResults.push({
                                    result: 'scrape url error:' + e,
                                    indexItem: {}
                                });
                                console.error('scrapeUrl  error: ' + e);
                            });
                    });
                }).catch(error => {
                    callback(new Error('get existing items error: ' + e));
                    console.log('get existingItems error', error);
                });
            }, function (e) {
                callback(new Error('findLinks error: ' + e));
                console.error('findLinks error: ' + e);
            });
    }, function (e) {
        callback(new Error('getHtml error: ' + e));
        console.error('getHtml error: ' + e);
    });
};

module.exports = { scrape };
