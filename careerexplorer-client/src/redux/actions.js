import { SHOW_ALL_FACETS_TOGGLE, LOADED_SEARCH_RESULTS, SEARCH_MENU_OPENED_TOGGLE, START_LOAD_SEARCH_RESULTS, SEARCH_QUERY_CHANGED, SEARCH_FACETS_CHANGED, SUBMIT_MENU_OPENED_TOGGLE } from "./actionTypes";

export const startLoadSearchResults = () => ({
    type: START_LOAD_SEARCH_RESULTS
});

export const loadedSearchResults = response => ({
    type: LOADED_SEARCH_RESULTS,
    payload: response
});

export const searchMenuOpenedToggle = enabled => ({
    type: SEARCH_MENU_OPENED_TOGGLE,
    payload: enabled
});

export const searchQueryChanged = query => ({
    type: SEARCH_QUERY_CHANGED,
    payload: query
});

export const searchFacetsChanged = facets => ({
    type: SEARCH_FACETS_CHANGED,
    payload: facets
});

export const submitMenuOpenedToggle = () => ({
    type: SUBMIT_MENU_OPENED_TOGGLE,
});

export const showAllFacetsToggle = () => ({
    type: SHOW_ALL_FACETS_TOGGLE,
});


