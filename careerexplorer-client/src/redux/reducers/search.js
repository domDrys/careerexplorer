import { SHOW_ALL_FACETS_TOGGLE, LOADED_SEARCH_RESULTS, SEARCH_MENU_OPENED_TOGGLE, START_LOAD_SEARCH_RESULTS, SEARCH_QUERY_CHANGED, SEARCH_FACETS_CHANGED } from "../actionTypes";

const initialState = {
    menuOpen: true,
    results: [],
    totalResults: 0,
    aggregations: [],
    query: {
        hiringOrganisation: null,
        education: {
            subject: ''
        },
        experiance: [],
        startIndex: 0
    },
    facets: {
        hiringOrganisation: null,
        education: null,
        experiance: []
    },
    isResultsLoaded: false,
    showAllFacets: false,
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SEARCH_MENU_OPENED_TOGGLE: {
            return {
                ...state,
                menuOpen: action.payload,
            }
        }
        case SHOW_ALL_FACETS_TOGGLE: {
            return {
                ...state,
                showAllFacets: !state.showAllFacets
            }
        }
        case LOADED_SEARCH_RESULTS: {
            return {
                ...state,
                results: state.query.startIndex === 0 ? action.payload.hits.hits : state.results.concat(action.payload.hits.hits),
                totalResults: action.payload.hits.total,
                aggregations: action.payload.aggregations,
                isResultsLoaded: state.isResultsLoaded || action.payload.hits.total > 0
            };
        }
        case START_LOAD_SEARCH_RESULTS: {
            return {
                ...state,
                menuOpen: false
            };
        }
        case SEARCH_QUERY_CHANGED: {
            return {
                ...state,
                query: Object.assign(state.query, action.payload)
            }
        }
        case SEARCH_FACETS_CHANGED: {
            return {
                ...state,
                facets: action.payload
            }
        }
        default:
            return state;
    }
}
