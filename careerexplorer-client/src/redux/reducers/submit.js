import { SUBMIT_MENU_OPENED_TOGGLE } from "../actionTypes";

const initialState = {
    menuOpen: false,
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SUBMIT_MENU_OPENED_TOGGLE: {
            return {
                ...state,
                menuOpen: !state.menuOpen,
            }
        }
        default:
            return state;
    }
}
