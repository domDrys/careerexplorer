import { combineReducers } from "redux";
import search from "./search";
import submit from "./submit";

export default combineReducers({ search, submit });
