import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from "react-redux";
import store from "./redux/store";
import ReactGA from 'react-ga';
import 'babel-polyfill';


if(process.env.REACT_APP_USE_ANALYTICS === 'true'){
    ReactGA.initialize('UA-129091036-1');
}

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

registerServiceWorker();
