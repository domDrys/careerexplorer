import React from "react";
import moment from "moment";
import keywords from '../keywords';
import ReactGA from 'react-ga';

const SearchResult = ({ id, title, postedDate, experiance, url, hiringOrganisation, imageUrl, highlight, aggregations, query, facets, score }) => {
    var icons = [];

    var hightlightKeys = [];
    if (highlight && highlight['experiance.subject']) {
        hightlightKeys = highlight['experiance.subject'].map((key) => key.replace(/<em>/g, '').replace(/<\/em>/g, ''));
    }

    if (highlight && highlight['experiance.keyword']) {
        hightlightKeys = hightlightKeys.concat(highlight['experiance.keyword'].map((key) => key.replace(/<em>/g, '').replace(/<\/em>/g, '')));
    }

    if (experiance && experiance.length > 0) {
        experiance.forEach(function (exp, i) {
            let matchIndex = -1;

            var style = null;
            var isMatch = false;

            if (hightlightKeys.some((highlight) => exp.subject && highlight === exp.subject)) {
                style = { order: matchIndex };
                isMatch = true;
            }

            //if is popular keyword, show before others
            if (!style && aggregations && aggregations.experianceSubject && aggregations.experianceSubject.buckets) {
                if (aggregations.experianceSubject.buckets.some((agg) => agg.key === exp.subject)) {
                    style = { order: 100 };
                }
            }

            //if is a known keyword, show before others
            if (!style && keywords) {
                if (keywords.some((key) => key === exp.subject)) {
                    style = { order: 200 };
                }
            }

            icons.push(<div key={`exp-${exp.subject}-${i}`} style={style}
                className={'search-result-icon search-result-icon-experiance ' + (isMatch ? 'search-result-icon-match' : '')}>
                {exp.subject}
            </div>);
        });
    }

    var timeSinceLabel = null;
    if (postedDate) {
        var numDays = moment().diff(moment(postedDate), 'days');
        if (numDays <= 0) {
            timeSinceLabel = "Posted today";
        } else if (numDays === 1) {
            timeSinceLabel = "Posted 1 day ago";
        } else {
            timeSinceLabel = `Posted ${numDays} days ago`;
        }
    }

    var onClickResult = () => ReactGA.event({
        category: 'search',
        action: 'clickedResult',
    });

    return <div className="search-result">
        {imageUrl ? <img src={imageUrl} className="search-result-logo" alt={`${hiringOrganisation} company logo`} /> : null}
        <div className="search-result-meta">
            <div className="search-result-org">{hiringOrganisation}</div>
            {timeSinceLabel ? <div className="search-result-date">{timeSinceLabel}</div> : null}
        </div>
        <a className="search-result-title" href={url} target="_blank" rel="noopener noreferrer" onClick={onClickResult}>{title}</a>
        <div className="search-result-icons">{icons}</div>
    </div>;
}

export default SearchResult;
