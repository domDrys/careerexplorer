import React, { Component } from 'react';
import { connect } from "react-redux";
import SearchResult from "./SearchResult";
import SearchResultPlaceholder from "./SearchResultPlaceholder";
import SearchAggregation from "./SearchAggregation";
import SearchAggregationPlaceholder from './SearchAggregationPlaceholder';
import { loadedSearchResults, startLoadSearchResults, searchFacetsChanged, showAllFacetsToggle, searchQueryChanged } from "../redux/actions";
import { faChevronCircleDown, faChevronCircleUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ReactGA from 'react-ga';
import Api from '../Api';

class SearchResults extends Component {
    constructor(props) {
        super(props);

        this.onLoadMore = this.onLoadMore.bind(this);
        this.onShowAllFacetsToggle = this.onShowAllFacetsToggle.bind(this);
    }
    onLoadMore() {
        this.props.startLoadSearchResults();
        var q = this.props.search.query;

        Api.search({
            hiringOrganisation: q.hiringOrganisation,
            experiance: q.experiance,
            education: q.education,
            location: q.location,
            startIndex : q.startIndex + 20
        }, this.props.search.facets, (response) => this.props.loadedSearchResults(response));

        this.props.searchQueryChanged({
            startIndex : q.startIndex + 20
        });

        ReactGA.event({
            category: 'search',
            action: 'query',
            label: 'load more'
        });
    }
    onShowAllFacetsToggle(){
        this.props.showAllFacetsToggle();
        ReactGA.event({
            category: 'search',
            action: 'showAllFacets',
        });
    }
    render() {
        if (this.props.submit.menuOpen) {
            return null;
        }

        var search = this.props.search;

        var items = [];
        var aggregations = [];
        if (!this.props.search.isResultsLoaded) {
            for (var i = 0; i < 20; i++) {
                items.push(<SearchResultPlaceholder key={i} />);
            }
            for (var j = 0; j < 20; j++) {
                aggregations.push(<SearchAggregationPlaceholder key={j} />)
            }
        } else {
            if (search.results) {
                items = search.results.map((result) => <SearchResult key={result._id} id={result._id} {...result._source} highlight={result.highlight} query={search.query} facets={search.facets} score={result._score} aggregations={search.aggregations} />);
            }

            if (search.aggregations) {
                var aggs = search.aggregations;

                if (aggs.hiringOrganisation && aggs.hiringOrganisation.buckets.length > 1) {
                    var hiringOrgAggs = aggs.hiringOrganisation.buckets.map((hiringOrg) =>
                        <SearchAggregation name={hiringOrg.key} key={`hiring-${hiringOrg.key}`} count={hiringOrg.doc_count} facetType="hiringOrganisation" />
                    );

                    aggregations = aggregations.concat(hiringOrgAggs);
                }

                if (aggs.experianceSubject && aggs.experianceSubject.buckets.length > 1 && search.query.experiance) {
                    var expAggs = aggs.experianceSubject.buckets
                        .filter((b) => !search.query.experiance.some((x) => x.subject === b.key) && !search.facets.experiance.some((x) => x === b.key))
                        .map((exp) =>
                            <SearchAggregation name={exp.key} key={`exp-${exp.key}`} count={exp.doc_count} facetType="experiance" />
                        );

                    aggregations = aggregations.concat(expAggs);
                }
            }
        }

        var showAllFacetsClassName = search.showAllFacets ? 'app-search-results-aggs-all' : '';

        var loadMoreBtn = null;
        if (search.isResultsLoaded && search.totalResults > items.length) {
            loadMoreBtn = <button className="app-search-results-load-more-btn" onClick={this.onLoadMore}>Load more</button>;
        }

        return <div className="app-search-results">
            <button className="app-search-results-aggs-toggle" onClick={this.onShowAllFacetsToggle}>
                {search.showAllFacets ? <FontAwesomeIcon icon={faChevronCircleUp} /> : <FontAwesomeIcon icon={faChevronCircleDown} />}
            </button>
            <div className="app-search-results-total">{search.totalResults} jobs</div>
            <div className={'app-search-results-aggs ' + showAllFacetsClassName}>
                {aggregations}
            </div>
            {items}
            {loadMoreBtn}
        </div >;
    }
};

const mapStateToProps = state => {
    return { ...state };
};

export default connect(mapStateToProps, { loadedSearchResults, startLoadSearchResults, searchFacetsChanged, showAllFacetsToggle, searchQueryChanged })(SearchResults);