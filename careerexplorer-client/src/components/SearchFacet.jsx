import React, { Component } from 'react';
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Api from '../Api';
import { loadedSearchResults, startLoadSearchResults, searchFacetsChanged } from "../redux/actions";
import { connect } from "react-redux";

class SearchFacet extends Component {
    constructor(props) {
        super(props);

        this.onRemove = this.onRemove.bind(this);
    }
    onRemove() {
        this.props.startLoadSearchResults();

        var newFacets = null;

        switch (this.props.facetType) {
            case 'hiringOrganisation':
                newFacets = {
                    hiringOrganisation: null
                };
                break;
            case 'experiance':
                newFacets = {
                    experiance: this.props.search.facets.experiance.filter((exp) => exp !== this.props.name)
                };
                break;
            default:
                break;
        }
        const facets = Object.assign(this.props.search.facets, newFacets);

        Api.search(this.props.search.query, facets, (response) => this.props.loadedSearchResults(response));

        this.props.searchFacetsChanged(facets);
    }
    render() {
        return <button className="search-form-facet" onClick={this.onRemove}>
            <div className="search-form-facet-label">{this.props.name}</div>
            <FontAwesomeIcon icon={faTimes} />
        </button>;
    }
};

const mapStateToProps = state => {
    return { ...state };
};

export default connect(mapStateToProps, { loadedSearchResults, startLoadSearchResults, searchFacetsChanged })(SearchFacet);


