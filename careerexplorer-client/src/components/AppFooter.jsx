import React, { Component } from 'react';
import { connect } from "react-redux";
import { submitMenuOpenedToggle } from "../redux/actions";
import { Link } from "react-router-dom";

class AppFooter extends Component {
    render() {
        return <div className="app-footer">
            <Link className="app-footer-post-job" to="/submit">Post a job</Link>
            <a className="app-footer-mailto" href="mailto:vancouvertechjobs@gmail.com">Contact us</a>
            <div className="app-footer-by">Built by <a href="https://www.linkedin.com/in/domdrysdale/">Dominic Drysdale</a></div>
        </div>;
    }
}

const mapStateToProps = state => {
    return { ...state };
};

export default connect(mapStateToProps, { submitMenuOpenedToggle })(AppFooter);