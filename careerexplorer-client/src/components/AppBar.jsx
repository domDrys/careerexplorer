import React, { Component } from 'react';
import { connect } from "react-redux";
import { submitMenuOpenedToggle } from "../redux/actions";
import ReactGA from 'react-ga';
import AppIcon from './AppIcon';

class AppBar extends Component {
    constructor(props) {
        super(props);

        this.onSubmitJobToggle = this.onSubmitJobToggle.bind(this);
    }
    onSubmitJobToggle() {
        this.props.submitMenuOpenedToggle();

        if (!this.props.submit.menuOpen) {
            ReactGA.event({
                category: 'submit-job',
                action: 'opened',
            });
        }
    }
    render() {
        return (
            <div className="app-bar">
                <div className="app-bar-content">
                    <h1 className="App-title">
                        <AppIcon />
                        vantechjobs
                        </h1>
                    <h2 className="App-description">vancouver tech industry jobs</h2>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { ...state };
};

export default connect(mapStateToProps, { submitMenuOpenedToggle })(AppBar);