import { debounce } from "debounce";
import React, { Component } from 'react';
import Api from '../Api';
import { connect } from "react-redux";
import { loadedSearchResults, startLoadSearchResults, searchQueryChanged } from "../redux/actions";
import SearchFacet from './SearchFacet';
import ReactGA from 'react-ga';

class SearchForm extends Component {
    constructor(props) {
        super(props);

        this.onChangeExperianceText = this.onChangeExperianceText.bind(this);
        this.onChangeEducationText = this.onChangeEducationText.bind(this);
        this.onClickSearch = debounce(this.onClickSearch.bind(this), 500);
        this.onChangeHiringOrg = this.onChangeHiringOrg.bind(this);
        this.onChangeLocation = this.onChangeLocation.bind(this);

        this.state = {
            experiance: [],
            tags: [],
            education: {
                level: 0,
                subject: ''
            },
            experianceInput: '',
            hiringOrganisation: '',
            locationInput: '',
        };
        this.clickedSearchTimeout = null;
    }
    componentDidMount() {
        if (this.props.urlQuery && this.props.urlQuery.q) {
            this.onChangeExperianceText({ target: { value: this.props.urlQuery.q } });
        } else {
            this.onClickSearch({});
        }
    }
    onChangeExperianceText(e) {
        var experiance = e.target.value.split(',').filter((x) => x.length > 1).map(function (subject) {
            return { subject: subject.trim().toLowerCase(), amount: 0, }
        });
        this.onClickSearch({ experiance });
        this.setState({
            experianceInput: e.target.value
        });
        this.props.history.push(`/?q=${e.target.value}`);
    }
    onChangeEducationText(e) {
        this.setState({
            education: {
                subject: e.target.value,
                level: this.state.education.level
            }
        });
        this.onClickSearch({ education: { subject: e.target.value, level: 0 } });
    }
    onChangeLocation(e) {
        this.setState({
            locationInput: e.target.input
        });
        this.onClickSearch({ location: e.target.value });
    }
    onChangeHiringOrg(e) {
        this.onClickSearch({ hiringOrganisation: e.target.value });
        this.setState({
            hiringOrganisation: e.target.value
        });
    }
    onClickSearch(newQuery) {
        this.props.startLoadSearchResults();
        var oldQuery = this.props.search.query;

        var hiringOrganisation = (typeof newQuery.hiringOrganisation !== 'undefined') ? newQuery.hiringOrganisation : oldQuery.hiringOrganisation;
        var experiance = (typeof newQuery.experiance !== 'undefined') ? newQuery.experiance : oldQuery.experiance;
        var education = (typeof newQuery.education !== 'undefined') ? newQuery.education : oldQuery.education;
        var location = (typeof newQuery.location !== 'undefined') ? newQuery.location : oldQuery.location;

        Api.search({ hiringOrganisation, experiance, education, location, startIndex: 0 }, this.props.search.facets, (response) => this.props.loadedSearchResults(response));

        this.props.searchQueryChanged({
            hiringOrganisation, education, experiance, startIndex: 0
        });

        if (experiance.length > 0) {
            experiance.forEach(function (exp) {
                ReactGA.event({
                    category: 'search',
                    action: 'query',
                    label: exp.subject
                });
            });
        } else {
            ReactGA.event({
                category: 'search',
                action: 'query',
                label: 'all'
            });
        }
    }
    render() {
        if (this.props.submit.menuOpen) {
            return null;
        }

        var hiringOrganisation = null;
        if (this.props.search.facets.hiringOrganisation) {
            hiringOrganisation = <div className="search-form-section">
                <SearchFacet name={this.props.search.facets.hiringOrganisation} facetType="hiringOrganisation" />
            </div>;
        } else {
            hiringOrganisation = <div className="search-form-section">
                <input className="search-form-input" type="text" placeholder="Hiring organisation" value={this.state.hiringOrganisation} onChange={this.onChangeHiringOrg}></input>
            </div>;
        }

        var experianceFacets = null;
        if (this.props.search.facets.experiance) {
            experianceFacets = this.props.search.facets.experiance.map((exp) =>
                <SearchFacet key={exp} name={exp} facetType="experiance" />);
        }

        return (
            <div className="search-form">
                <div className="search-form-section search-form-section-experiance">
                    <textarea className="search-form-input" placeholder="Type multiple keywords eg: 'react, javascript, AWS...'" value={this.state.experianceInput} onChange={this.onChangeExperianceText} />
                    {experianceFacets}
                </div>
                {hiringOrganisation}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { ...state };
};

export default connect(mapStateToProps, { loadedSearchResults, startLoadSearchResults, searchQueryChanged })(SearchForm);