import React from "react";
import SearchForm from './SearchForm';
import SearchResults from './SearchResults';
import qs from 'qs';

const Search = ({ location, history }) => {
    const urlQuery = location.search ? qs.parse(location.search) : null;

    if(urlQuery && urlQuery['?q']){
        urlQuery.q = urlQuery['?q'];
    }

    return <React.Fragment>
        <SearchForm urlQuery={urlQuery} history={history} />
        <SearchResults />
    </React.Fragment>;
}


export default Search;

