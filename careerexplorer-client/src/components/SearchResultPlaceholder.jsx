import React from "react";
import Skeleton from './Skeleton';

const SearchResultPlaceholder = () => {
    var icons = [];
    for (var i = 0; i < 6; i++) {
        icons.push(<div key={i} className="search-result-icon search-result-icon-experiance">
            <Skeleton  />
        </div >);
    }

    return <div className="search-result search-result-placeholder">
        <div className="search-result-logo"><Skeleton /></div>
        <div className="search-result-meta">
            <div className="search-result-org"><Skeleton /></div>
            <div className="search-result-date"><Skeleton /></div>
        </div>
        <div className="search-result-title"><Skeleton /></div>
        <div className="search-result-icons">{icons}</div>
    </div>;
}

export default SearchResultPlaceholder;
