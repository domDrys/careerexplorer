import React from "react";
import Skeleton from './Skeleton';

const SearchAggregationPlaceholder = () => {
    return <button className="app-search-results-aggs-item app-search-results-aggs-item-placeholder" style={{ order: -1000 }}>
        <Skeleton />
    </button>;
}

export default SearchAggregationPlaceholder;
