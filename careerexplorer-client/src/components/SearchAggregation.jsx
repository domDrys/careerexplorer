import React, { Component } from 'react';
import Api from '../Api';
import { loadedSearchResults, startLoadSearchResults, searchFacetsChanged, searchQueryChanged } from "../redux/actions";
import { connect } from "react-redux";
import ReactGA from 'react-ga';

class SearchAggregation extends Component {
    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
    }
    onClick() {
        this.props.startLoadSearchResults();
        var newFacets = null;
        switch (this.props.facetType) {
            case 'hiringOrganisation':
                newFacets = {
                    hiringOrganisation: this.props.name
                };
                ReactGA.event({
                    category: 'search',
                    action: 'addHiringOrgFacet',
                    label: this.props.name
                });
                break;
            case 'experiance':
                newFacets = {
                    experiance: this.props.search.facets.experiance.concat(this.props.name)
                };
                ReactGA.event({
                    category: 'search',
                    action: 'addExperianceFacet',
                    label: this.props.name
                });
                break;
            default:
                break;
        }

        const facets = Object.assign(this.props.search.facets, newFacets);

        var query = Object.assign({}, this.props.search.query);

        query.startIndex = 0;

        Api.search(query, facets, (response) => this.props.loadedSearchResults(response));

        this.props.searchFacetsChanged(facets);
        this.props.searchQueryChanged({
            startIndex: 0
        });        
    }
    render() {
        var count = this.props.count;
        var name = this.props.name;

        var label = null;
        switch (this.props.facetType) {
            case 'hiringOrganisation':
                label = `${count} at ${name}`;
                break;
            case 'experiance':
                label = `${count} ${name}`;
                break;
            default:
                break;
        }

        return <button className="app-search-results-aggs-item" onClick={this.onClick} style={{ order: -count }}>
            {label}
        </button>
    }
}
const mapStateToProps = state => {
    return { ...state };
};
export default connect(mapStateToProps, { loadedSearchResults, startLoadSearchResults, searchFacetsChanged, searchQueryChanged })(SearchAggregation);

