import React, { Component } from 'react';
import { connect } from "react-redux";
import SearchResult from "./SearchResult";
import Api from '../Api';
import { submitMenuOpenedToggle, } from "../redux/actions";
import ReCAPTCHA from "react-google-recaptcha";
import ReactGA from 'react-ga';
import { Link } from "react-router-dom";

class SubmitJob extends Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.onSubmitComplete = this.onSubmitComplete.bind(this);
        this.onUrlChange = this.onUrlChange.bind(this);
        this.onMetadataLoad = this.onMetadataLoad.bind(this);
        this.isFormValid = this.isFormValid.bind(this);
        this.onSubmitJobToggle = this.onSubmitJobToggle.bind(this);
        this.state = {
            locationInput: -1,
            isSubmitting: false,
            experianceInput: '',
            urlInput: '',
            jobTitleInput: '',
            organisationInput: '',
            imageUrl: '',
            validated: false,
            captchaValue: null
        };
    }
    onSubmitJobToggle() {
        this.props.submitMenuOpenedToggle();

        if (!this.props.submit.menuOpen) {
            ReactGA.event({
                category: 'submit-job',
                action: 'opened',
            });
        }
    }
    onSubmit() {
        var job = {
            experiance: this.state.experianceInput,
            url: this.state.urlInput,
            jobTitle: this.state.jobTitleInput,
            hiringOrganisation: this.state.organisationInput,
            location: this.state.locationInput,
            imageUrl: this.state.imageUrl,
            captchaValue: this.state.captchaValue,
        };

        Api.postJob(job, this.onSubmitComplete);
        this.setState({
            isSubmitting: true
        });
    }
    onSubmitComplete(success) {
        if (!success) {
            this.setState({
                isSubmitting: false
            });
        } else {
            this.props.submitMenuOpenedToggle();
            this.setState({
                locationInput: -1,
                isSubmitting: false,
                experianceInput: '',
                urlInput: '',
                jobTitleInput: '',
                organisationInput: '',
                imageUrl: ''
            });

            ReactGA.event({
                category: 'submit-job',
                action: 'submitted',
            });
        }
    }
    onUrlChange(e) {
        this.setState({
            urlInput: e.target.value
        });

        Api.fetchMetadata(e.target.value, this.onMetadataLoad);
    }
    onMetadataLoad(metadata) {
        var nextState = {
            validated: false
        };

        if (metadata.title) {
            nextState.validated = true;
        }

        if (metadata.ogImage && metadata.ogImage.length > 1) {
            nextState.imageUrl = metadata.ogImage;
        }

        if (metadata.ogTitle && metadata.ogTitle.length > 1) {
            nextState.jobTitleInput = metadata.ogTitle;
        } else if (metadata.title) {
            nextState.jobTitleInput = metadata.title;
        }

        this.setState(nextState)
    }
    isFormValid() {
        if (this.state.experianceInput.length < 3) {
            return false;
        }

        if (this.state.locationInput.length < 3) {
            return false;
        }

        if (this.state.organisationInput.length < 3) {
            return false;
        }

        if (this.state.jobTitleInput.length < 3) {
            return false;
        }

        if (this.state.urlInput.length < 3) {
            return false;
        }

        return true;
    }
    render() {
        var experiance = [];
        if (this.state.experianceInput) {
            experiance = this.state.experianceInput.split(',').filter((x) => x.length > 1).map(function (subject) {
                return { subject: subject.trim(), amount: 0, }
            });
        } else {
            var experiancePlaceholder = 'Lorem ipsum, dolor, sit, amet, consectetur adipiscing, elit, sed do, eiusmod, tempor incididunt, labore, dolore magna, aliqua';
            experiance = experiancePlaceholder.split(',').filter((x) => x.length > 1).map(function (subject) {
                return { subject: subject.trim(), amount: 0, }
            });
        }
        return <div className="app-submit-job">
            <div className="app-submit-job-form">
                <input disabled={this.state.isSubmitting} className="app-submit-job-form-element" placeholder="Paste URL here" value={this.state.urlInput} onChange={this.onUrlChange} />
                <input disabled={this.state.isSubmitting} className="app-submit-job-form-element" placeholder="My job title" value={this.state.jobTitleInput} onChange={(e) => this.setState({ jobTitleInput: e.target.value })} />
                <input disabled={this.state.isSubmitting} className="app-submit-job-form-element" placeholder="My company name" value={this.state.organisationInput} onChange={(e) => this.setState({ organisationInput: e.target.value })} />
                <div className="app-submit-job-form-element" >
                    <span>Location</span>
                    <select value={this.state.locationInput} onChange={(e) => this.setState({ locationInput: e.target.value })}>
                        <option value={-1} disabled>Select</option>
                        <option value='vancouver, bc, canada'>Vancouver</option>
                        <option value='burnaby, bc, canada'>Burnaby</option>
                        <option value='richmond, bc, canada'>Richmond</option>
                    </select>
                </div>
                <textarea disabled={this.state.isSubmitting} className="app-submit-job-form-element" placeholder="Type relevant experiance keywords, eg: 'software development, project management, accounting'"
                    value={this.state.experianceInput} onChange={(e) => this.setState({ experianceInput: e.target.value })} />
                <ReCAPTCHA sitekey="6LfD73kUAAAAAAPpKHvBY2C5QD6QY9XdGEDrRvQQ" onChange={(captchaValue) => this.setState({ captchaValue })} />
                <Link className="app-submit-job-form-cancel" to="/">Cancel</Link>
                <button disabled={this.state.isSubmitting || !this.isFormValid()} className="app-submit-job-form-submit" onClick={this.onSubmit}>Submit Job</button>
            </div>
            <SearchResult
                title={this.state.jobTitleInput || 'My job title'}
                postedDate={new Date().toISOString()}
                experiance={experiance}
                url={this.state.urlInput}
                hiringOrganisation={this.state.organisationInput || 'My company name'}
                imageUrl={this.state.imageUrl} />
        </div>
    }
}

const mapStateToProps = state => {
    return { ...state };
};

export default connect(mapStateToProps, { submitMenuOpenedToggle })(SubmitJob);