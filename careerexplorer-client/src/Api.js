const API_URL = process.env.REACT_APP_API_URL;

function search(query, facets, onComplete) {
    var hiringOrganisation = query.hiringOrganisation;
    var experiance = query.experiance;
    var education = query.education;
    var location = query.location;
    var startIndex = query.startIndex;

    post(`${API_URL}/search`, { hiringOrganisation, experiance, education, location, startIndex, facets }, onComplete);
}

function postJob(job, onComplete) {
    console.log('post job', job);

    post(`${API_URL}/submit`, job, onComplete);
}

function fetchMetadata(url, onComplete) {
    post(`${API_URL}/submit/metadata`, { url }, onComplete);
}

function post(url, body, onComplete) {
    fetch(url, {
        method: "POST",
        headers: { "Content-Type": "application/json; charset=utf-8", },
        body: JSON.stringify(body),
    })
        .then(function (response) { return response.json(); })
        .then(function (data) {
            onComplete(data);
        })
        .catch(error => console.error(error));
}

export default { search, postJob, fetchMetadata };