import React, { Component } from 'react';
import SubmitJob from './components/SubmitJob';
import './App.css';
import AppBar from './components/AppBar';
import AppFooter from './components/AppFooter';
import Search from './components/Search';
import { BrowserRouter as Router, Route } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <header>
            <AppBar />
          </header>
          <div className="app-main">
            <Route path="/submit" exact component={SubmitJob} />
            <Route path="/" exact component={Search} />
          </div>
          <AppFooter />
        </div>
      </Router>
    );
  }
}

export default App;
