#Career Explorer
##Description
This is a simple experimental app intented to provide a better job search, by allowing users to search for jobs using all of their experiance, education & skills.

##Projects

###client 
a simple UI React / Redux app
###api 
receieves requests from client, generates queries for ElasticSearch

###scraper
 visits the job page for various companies, extracts HTML & parses job data to be pushed to ElasticSearch.

##more interesting scripts:
###careerexplorer-api\src\es\queryBuilder.js
This class translates requests from the client to ElasticSearch queries


